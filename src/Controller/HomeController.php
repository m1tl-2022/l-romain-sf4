<?php

namespace App\Controller;

use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param RoomRepository $roomRepository
     */
    public function index(): Response
    {

        // $rooms = $roomRepository->findAll();
        //     ->find($id)
        //     ->findOneBy($array)
        //     ->findAll();
       
        // return new RedirectResponse($this->urlGenerator->generate('home'));
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            // 'rooms' =>$rooms,
        ]);
    }
   
}

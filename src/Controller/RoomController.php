<?php

namespace App\Controller;

use App\Form\RoomType;
use App\Repository\RoomRepository;
use App\Entity\Room;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class RoomController extends AbstractController
{

    private $security;

    public function __construct(security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/room", name="room")
     */
    public function index(RoomRepository $roomRepository): Response
    {
        $totalRooms = $roomRepository->findAll();
        $rooms = $roomRepository->findBy(['isAvailable' => 'true']);
        $count = count($rooms);
        $total = count($totalRooms);
        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms' => $rooms,
            'count' => $count,
            'total' => $total
        ]);
    }

    /**
     * @Route("/new", name="room.add", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     *
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $room->setCreatedAt(new DateTime());
        $room->setUser($this->security->getUser());
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('success', 'la salle à bien été ajoutée');
            return $this->redirectToRoute('room');
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="room.edit", methods={"GET", "POST"})
     * @IsGranted ("ROLE_USER")
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);
        $room->setUpdateAtAt(new DateTime());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La modification de la salle a bien ete enregsitré');
            return $this->redirectToRoute('room');
        }
        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/{id}", name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     *
     */
    public function delete(Request $request, Room $room):Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success', 'La salle a bien été supprimée.');
        }
        return $this->redirectToRoute('room');
    }
}




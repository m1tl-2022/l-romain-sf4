<?php

namespace App\Repository;

use App\Entity\IsAvailable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IsAvailable|null find($id, $lockMode = null, $lockVersion = null)
 * @method IsAvailable|null findOneBy(array $criteria, array $orderBy = null)
 * @method IsAvailable[]    findAll()
 * @method IsAvailable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IsAvailableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IsAvailable::class);
    }

    // /**
    //  * @return IsAvailable[] Returns an array of IsAvailable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IsAvailable
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
